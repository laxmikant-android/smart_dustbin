package com.smart.dustbin.viewmodel;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.smart.dustbin.base.BaseViewModel;
import com.smart.dustbin.models.repositories.UserDetailsRepository;

import java.util.List;

public class UserDetailViewModel extends BaseViewModel {

    private UserDetailsRepository mUserRepo;


    public UserDetailViewModel(@NonNull Application application, UserDetailsRepository repository) {
        super(application);
        mUserRepo = repository;
    }

    /*public LiveData<Resource<AuthTokenResponse>> loginUser(LoginRequest request) {
        return mUserRepo.loginUser(request);
    }
    */
}
