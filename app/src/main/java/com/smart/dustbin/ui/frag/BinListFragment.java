package com.smart.dustbin.ui.frag;


import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.smart.dustbin.R;
import com.smart.dustbin.base.BaseFragment;
import com.smart.dustbin.databinding.FragmentBinListBinding;
import com.smart.dustbin.ui.BinDetailActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class BinListFragment extends BaseFragment implements View.OnClickListener {


    private FragmentBinListBinding binding;

    public BinListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_bin_list, container, false);

        binding.btn.setOnClickListener(this);
        return binding.getRoot();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn:
                startActivity(new Intent(getActivity(), BinDetailActivity.class));
                break;
        }
    }
}
