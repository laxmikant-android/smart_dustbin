package com.smart.dustbin.ui;

import android.app.DatePickerDialog;
import android.databinding.DataBindingUtil;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;

import com.smart.dustbin.R;
import com.smart.dustbin.base.BaseActivity;
import com.smart.dustbin.databinding.ActivityBinDetailBinding;

import java.util.Calendar;

public class BinDetailActivity extends BaseActivity implements View.OnClickListener {

    private ActivityBinDetailBinding binding;

    private DatePickerDialog datePicker;
    private Calendar calendar;
    private int year, month, day;
    private int selection_date_type = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_bin_detail);
        binding.setActivity(this);
        configureToolBar(binding.myToolbar.toolbar);
        binding.myToolbar.toolbar.setNavigationIcon(ContextCompat.getDrawable(this, R.drawable.ic_arrow_back_black_24dp));

        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);

        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        showDate(year, month+1, day);

        datePicker = new DatePickerDialog(this, myDateListener, year, month, day);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.tv_start_date:
                selection_date_type =1;
                datePicker.show();
                break;
            case R.id.tv_end_date:
                selection_date_type =2;
                datePicker.show();
                break;
        }
    }

    private DatePickerDialog.OnDateSetListener myDateListener = new
            DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker arg0,
                                      int arg1, int arg2, int arg3) {
                    // TODO Auto-generated method stub
                    // arg1 = year
                    // arg2 = month
                    // arg3 = day
                    showDate(arg1, arg2 + 1, arg3);
                }
            };

    private void showDate(int year, int month, int day) {
        if (selection_date_type == 1) {
            binding.tvStartDate.setText(new StringBuilder().append("Start Date :").append(day).append("/")
                    .append(month).append("/").append(year));
        } else if (selection_date_type == 2) {
            binding.tvEndDate.setText(new StringBuilder().append("End Date :").append(day).append("/")
                    .append(month).append("/").append(year));
        }

    }

}
