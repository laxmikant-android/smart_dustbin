package com.smart.dustbin.ui.customviews;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;

import com.smart.dustbin.R;
import com.smart.dustbin.utils.IAlertDialogActionListener;


/**
 * Created by kipl146 on 5/12/2017.
 */

public class SimpleDialogFragment extends DialogFragment {

    private IAlertDialogActionListener iAlertDialogActionListener;

    public void setIActionSelectionListener(IAlertDialogActionListener iAlertDialogActionListener) {
        this.iAlertDialogActionListener = iAlertDialogActionListener;
    }

    public static SimpleDialogFragment newInstance(String title, String message, String action1, String action2, String action3) {
        SimpleDialogFragment frag = new SimpleDialogFragment();
        Bundle args = new Bundle();
        if (!TextUtils.isEmpty(title)) {
            args.putString("title", title);
        }
        if (!TextUtils.isEmpty(message)) {
            args.putString("message", message);
        }
        if (!TextUtils.isEmpty(action1)) {
            args.putString("action1", action1);
        }
        if (!TextUtils.isEmpty(action2)) {
            args.putString("action2", action2);
        }
        if (!TextUtils.isEmpty(action3)) {
            args.putString("action3", action3);
        }
        frag.setArguments(args);
        return frag;
    }

    @Override
    public AlertDialog onCreateDialog(Bundle savedInstanceState) {

        String title = "";
        String message = "";
        String action1 = "";
        String action2 = "";
        String action3 = "";

        if (getArguments() != null) {
            if (getArguments().containsKey("title")) {
                title = getArguments().getString("title");
            }
            if (getArguments().containsKey("message")) {
                message = getArguments().getString("message");
            }
            if (getArguments().containsKey("action1")) {
                action1 = getArguments().getString("action1");
            }
            if (getArguments().containsKey("action2")) {
                action2 = getArguments().getString("action2");
            }
            if (getArguments().containsKey("action3")) {
                action3 = getArguments().getString("action3");
            }
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AlertDialogTheme);
        builder.setCancelable(false).setTitle(title).setMessage(message);

        if (!TextUtils.isEmpty(action1)) {
            builder.setPositiveButton(action1,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            if (iAlertDialogActionListener != null) {
                                iAlertDialogActionListener.onActionSelected(1);
                            }
                        }
                    }
            );

        }

        if (!TextUtils.isEmpty(action2)) {
            builder.setNegativeButton(action2,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            if (iAlertDialogActionListener != null) {
                                iAlertDialogActionListener.onActionSelected(2);
                            }
                        }
                    }
            );
        }

        if (!TextUtils.isEmpty(action3)) {
            builder.setNeutralButton(action3,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            if (iAlertDialogActionListener != null) {
                                iAlertDialogActionListener.onActionSelected(3);
                            }
                        }
                    }
            );

        }
        AlertDialog alertDialog = builder.create();

       /* alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Button negButton = ((AlertDialog) dialog).getButton(DialogInterface.BUTTON_NEGATIVE);
                negButton.setBackgroundColor(getResources().getColor(R.color.white));
                negButton.setTextColor(getResources().getColor(R.color.black));

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                );
                params.setMargins(20,0,0,0);
                negButton.setLayoutParams(params);
            }
        });*/

        return alertDialog;
    }



}
