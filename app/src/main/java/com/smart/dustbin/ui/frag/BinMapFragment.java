package com.smart.dustbin.ui.frag;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.smart.dustbin.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class BinMapFragment extends Fragment {


    public BinMapFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_bin_map, container, false);
    }

}
