package com.smart.dustbin.ui.customviews;

import android.content.Context;
import android.graphics.Rect;
import android.support.annotation.DimenRes;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class ItemOffsetDecoration extends RecyclerView.ItemDecoration {

    private int mItemOffsetLeft;
    private int mItemOffsetRight;
    private int mItemOffsetTop;
    private int mItemOffsetBottom;

    public ItemOffsetDecoration(int itemOffset) {
        mItemOffsetLeft = itemOffset;
        mItemOffsetRight = itemOffset;
        mItemOffsetTop = itemOffset;
        mItemOffsetBottom = itemOffset;
    }

    public ItemOffsetDecoration(int left, int right , int top, int bottom) {
        mItemOffsetLeft = left;
        mItemOffsetRight = right;
        mItemOffsetTop = top;
        mItemOffsetBottom = bottom;
    }

    public ItemOffsetDecoration(@NonNull Context context, @DimenRes int itemOffsetId) {
        this(context.getResources().getDimensionPixelSize(itemOffsetId));
    }

    public ItemOffsetDecoration(@NonNull Context context, @DimenRes int left,@DimenRes int right
            ,@DimenRes int top,@DimenRes int bottom) {
        this(context.getResources().getDimensionPixelSize(left),
                context.getResources().getDimensionPixelSize(right),
                context.getResources().getDimensionPixelSize(top),
                context.getResources().getDimensionPixelSize(bottom));
    }
    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                               RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);
        outRect.set(mItemOffsetLeft, mItemOffsetTop, mItemOffsetRight, mItemOffsetBottom);
    }
}
