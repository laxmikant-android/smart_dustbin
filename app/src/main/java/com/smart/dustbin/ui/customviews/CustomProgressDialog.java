package com.smart.dustbin.ui.customviews;

import android.app.Dialog;
import android.content.Context;
import android.view.Window;
import android.widget.ProgressBar;

/**
 * Created by kipl146 on 5/9/2017.
 *
 * a custom dialog class to display a simple circular progress bar
 *
 */

public class CustomProgressDialog extends Dialog {

    public CustomProgressDialog(Context context, String text) {
        super(context, android.R.style.Theme_Holo_Dialog_NoActionBar);
        ProgressBar v = new ProgressBar(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(v);
        setCancelable(false);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
    }

}
