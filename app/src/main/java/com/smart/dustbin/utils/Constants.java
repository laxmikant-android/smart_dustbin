package com.smart.dustbin.utils;

import java.util.ArrayList;
import java.util.Arrays;

public interface Constants {
    String success = "true";
    String error = "false";
    String loading = "loading";
    String DropDown = "dropdown";
    String Image = "image";
    String COLOR = "color";
    String SIZE = "size";
    String VARIABLE = "variable";
    String New = "date";
    String Popular = "popularity";
    String Price = "price";
    String HighToLow = "desc";
    String LOwToHigh = "asc";
    String DateFormat_YYYY_MM_DDTHHMMSSZ = "yyyy-MM-dd'T'HH:mm:ss'Z'";
    //2019-09-11T08:33:44.251329Z
    String DateFormat_YYYY_MM_DDTHHMMSS_SSSSSSZ = "yyyy-MM-dd'T'HH:mm:ss.ssssss'Z'";
    String DateFormat_YYYY_MM_DDTHHMMSS_SSS = "yyyy-MM-dd'T'HH:mm:ss.sss";
    String DateFormat_EEEDYY = "MMM dd,yyyy";
    String DateFormat_1 = "yyyy-MM-dd HH:mm:ss";
    String DateFormat_2 = "EEE, dd  MMM yy | hh:mm a";
    String DateFormat_3 = "dd/MM/yy";
    String DateFormat_4 = "HH:mm:ss";

    //type of address
    String Billing = "billing";
    String Shipping = "shipping";

    ArrayList<String> news_category = new ArrayList<String>(Arrays.asList("entertainment", "sports",
            "technology",
            "health",
            "business"));
    String[] news_country = {"ng","za"};

}
