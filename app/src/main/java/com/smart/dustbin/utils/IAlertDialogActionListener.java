package com.smart.dustbin.utils;

/**
 * Created by kipl146 on 6/3/2017.
 */

public interface IAlertDialogActionListener {
    void onActionSelected(int actionType);
}
