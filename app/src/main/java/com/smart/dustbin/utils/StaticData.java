package com.smart.dustbin.utils;


import com.smart.dustbin.BuildConfig;

/**
 * Created by kipl146 on 6/2/2017.
 */

public class StaticData {

    public static boolean DEBUG = true;

    public static final String HEADER_APP_VERSION = BuildConfig.VERSION_NAME;
    public static final String HEADER_LANGUAGE = "en";
    public static final String HEADER_DEVICE_OS = "android";
    public static String HEADER_Country = "USA";

    /** APIs **/
    /**
     * Development Environment
     **/
    // public static final String BASE_URL = "";
    /**
     * Staging Environment
     **/
    public static final String BASE_URL = "http://18.224.246.143";
    /**
     * Production Environment
     **/
  //  public static final String BASE_URL = "";


}
