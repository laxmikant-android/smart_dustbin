package com.smart.dustbin.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Adminis on 8/21/2017.
 */

public class MySharedPreference {

    private static final String USER_PREFS = "LOCTAG_PREFS";
    private SharedPreferences appSharedPrefs;
    private SharedPreferences.Editor prefsEditor;
    private String welcomeVisited = "LocTagWelcomeVisited";
    private String language = "Language";
    private String userDetail = "UserDetail";
    private String userId = "UserId";
    private String userName = "UserName";
    private String loginStatus = "loginStatus";
    private String loginToken = "LoginToken";
    private String lat = "lat";
    private String lng = "lng";

    /* -------------------------------------*/
    public MySharedPreference(Context context) {
        this.appSharedPrefs = context.getSharedPreferences(USER_PREFS, Activity.MODE_PRIVATE);
        this.prefsEditor = appSharedPrefs.edit();
    }

    /** saves state if the Welcome Screen is visited **/

    public void setWelcomeVisited(boolean welcomeVisited) {
        prefsEditor.putBoolean(this.welcomeVisited, welcomeVisited).commit();
    }

    public boolean isWelcomeVisited() {
        return appSharedPrefs.getBoolean(welcomeVisited, false);
    }

    /* --------------User Email-----------------------*/
    public void setLanguage(String lang) {
        prefsEditor.putString(language, lang).commit();
    }

    public String getLanguage() {
        return appSharedPrefs.getString(language,"");
    }

    /* -------------- Login Status-----------------------*/
    public void setLoginStatus(boolean loginStatus) {
        prefsEditor.putBoolean(this.loginStatus, loginStatus).commit();
    }

    public boolean getLoginStatus() {
        return appSharedPrefs.getBoolean(loginStatus, false);
    }

    /* --------------User Detail-----------------------*/
    /*public void setUserDetail(UserModel userDetail) {

        setUserName(userDetail.getFullName());
        setUserContactNo(userDetail.getMobileNo());

        Gson gson = new Gson();
        Type type = new TypeToken<UserModel>() {}.getType();
        String json = gson.toJson(userDetail, type);

        prefsEditor.putString(this.userDetail, json).commit();
    }

    public UserModel getUserDetail() {
        String model = appSharedPrefs.getString(userDetail, "");
        Gson gson = new Gson();
        Type type = new TypeToken<UserModel>() {}.getType();

        return gson.fromJson(model, type);
    }*/

    public String getUserDetailString() {
        return appSharedPrefs.getString(userDetail, "");
    }

    /* --------------User ID-----------------------*/
    public void setUserId(String userId) {
        prefsEditor.putString(this.userId, userId).commit();
    }

    public String getUserID() {
        return appSharedPrefs.getString(userId, "");
    }

    /* --------------User Name-----------------------*/
    public void setUserName(String userName) {
        prefsEditor.putString(this.userName, userName).commit();
    }

    public String getUserName() {
        return appSharedPrefs.getString(userName, "");
    }

    /* -------------- Login Token-----------------------*/
    public void setLoginToken(String loginToken) {
        prefsEditor.putString(this.loginToken, loginToken).commit();
    }

    public String getLoginToken() {
        return appSharedPrefs.getString(loginToken, "");
    }

    /* -------------- Lat -----------------------*/
    public void setLat(long lat) {
        prefsEditor.putLong(this.lat, lat).commit();
    }

    public long getLat() {
        return appSharedPrefs.getLong(lat, 0);
    }

    /* -------------- Lng-----------------------*/
    public void setLng(long lng) {
        prefsEditor.putLong(this.lng, lng).commit();
    }

    public long getLng() {
        return appSharedPrefs.getLong(lng, 0);
    }


    /* --- Clear ----*/
    public void clear(){
        prefsEditor.clear().commit();

    }
}
