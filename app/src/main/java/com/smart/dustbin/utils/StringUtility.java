package com.smart.dustbin.utils;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.text.Html;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.widget.EditText;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.DecimalFormat;
import java.util.List;
import java.util.regex.Pattern;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class StringUtility {


    public static final String RAVE_PAYMENT = "NGN, USD, EUR, GBP, UGX, TZS, GHS, KES, ZAR";

    public static final String paypal_payment = "AUD, BRL, CAD, CZK, DKK, EUR, HKD, HUF, INR, ILS" +
            ", JPY, MYR, MXN, TWD, NZD, NOK, PHP, PLN, GBP, RUB, SGD, SEK, CHF, THB, USD";

    public static boolean stringNotNull(String str) {
        return str != null;
    }

    public static boolean stringNotEmpty(String str) {
        return !str.isEmpty();
    }

    public static boolean validateString(String str) {
        return stringNotNull(str) && stringNotEmpty(str);
    }

    public static boolean validateEditText(EditText editText) {
        return stringNotEmpty(editText.getText().toString().trim());
    }

    public static boolean validateMobileNumber(EditText mobileEditText) {
        /*final Pattern regexPattern = Pattern.compile("[789]\\d{9}");
        return regexPattern.matcher(mobileEditText.getText().toString().trim()).matches();*/

        if (mobileEditText.getText().toString().trim().length() == 10) {
            return true;
        }
        return false;
    }


    public static boolean validateEmail(EditText emailEditText) {
        final Pattern emailPattern = Pattern.compile("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+(aero|asia|biz|cat|com|coop|edu|gov|info|int|jobs|mil|mobi|museum|name|net|org|pro|tel|travel|ac|ad|ae|af|ag|ai|al|am|an|ao|aq|ar|as|at|au|aw|ax|az|ba|bb|bd|be|bf|bg|bh|bi|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|cr|cu|cv|cx|cy|cz|de|dj|dk|dm|do|dz|ec|ee|eg|er|es|et|eu|fi|fj|fk|fm|fo|fr|ga|gb|gd|ge|gf|gg|gh|gi|gl|gm|gn|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|im|in|io|iq|ir|is|it|je|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|me|mg|mh|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|mv|mw|mx|my|mz|na|nc|ne|nf|ng|ni|nl|no|np|nr|nu|nz|om|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|ps|pt|pw|py|qa|re|ro|rs|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|sk|sl|sm|sn|so|sr|st|su|sv|sy|sz|tc|td|tf|tg|th|tj|tk|tl|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zw)\\b", 2);
        return emailPattern.matcher(emailEditText.getText().toString().trim()).matches();
    }


    public static String getDoubleAsString(Float value) {
        if (value == null) {
            return "";
        }
        DecimalFormat df2 = new DecimalFormat("0.00");
        return df2.format(value);
    }

    public static boolean validateExpiryDate(EditText edtExpirationDate) {
        return true;
    }

    public static String setTitleCase(String text) {
        String result = "";
        if (!TextUtils.isEmpty(text)) {
            text = Html.fromHtml(text).toString();
            char firstChar = text.charAt(0);
            char firstCharToUpperCase = Character.toUpperCase(firstChar);
            result = result + firstCharToUpperCase;
            for (int i = 1; i < text.length(); i++) {
                char currentChar = text.charAt(i);
                char previousChar = text.charAt(i - 1);
                if (previousChar == ' ') {
                    char currentCharToUpperCase = Character.toUpperCase(currentChar);
                    result = result + currentCharToUpperCase;
                } else {
                    char currentCharToLowerCase = Character.toLowerCase(currentChar);
                    result = result + currentCharToLowerCase;
                }
            }

        }
        return result;
    }

    public static MultipartBody.Builder createMultiPartBuilder(String[] paramsName, String[] paramsValue, String[] FormName, List<String> imageArray) {
        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);

        for (int i = 0; i < paramsName.length; i++) {
            builder.addFormDataPart(paramsName[i], paramsValue[i]);
            Log.e("Quiz App", "createBuilder: " + paramsName[i] + paramsValue[i]);
        }
        if (imageArray.size() > 0) {
            for (int i = 0; i < imageArray.size(); i++) {
                File f = new File(imageArray.get(i));
                if (f.exists()) {
                    //   builder.addFormDataPart(FormName[i], TEMP_FILE_NAME + i + ".png", RequestBody.create(MediaType.parse("image/png"), f));
                    builder.addFormDataPart(FormName[0], "SmartBinApp" + i + ".png", RequestBody.create(MediaType.parse("image/png"), f));
                }
            }
        }
        Log.e("builder", "createBuilder: " + builder.toString());
        return builder;
    }

    public static String getPath(final Context context, final Uri uri) {
        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
        Log.i("URI", uri + "");
        String result = uri + "";
        // DocumentProvider
        //  if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
        if (isKitKat && (result.contains("media.documents"))) {
            String[] ary = result.split("/");
            int length = ary.length;
            String imgary = ary[length - 1];
            final String[] dat = imgary.split("%3A");
            final String docId = dat[1];
            final String type = dat[0];
            Uri contentUri = null;
            if ("image".equals(type)) {
                contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
            } else if ("video".equals(type)) {
            } else if ("audio".equals(type)) {
            }
            final String selection = "_id=?";
            final String[] selectionArgs = new String[]{
                    dat[1]
            };
            return getDataColumn(context, contentUri, selection, selectionArgs);
        } else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }

    public static String getDataColumn(Context context, Uri uri, String selection, String[] selectionArgs) {
        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };
        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    public static String ImageToString(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        byte[] imagByte = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(imagByte, Base64.DEFAULT);
    }
}
