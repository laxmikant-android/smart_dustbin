package com.smart.dustbin;

import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.smart.dustbin.apis.ApiService;
import com.smart.dustbin.utils.NetworkUtility;
import com.smart.dustbin.utils.PreferenceUtils;
import com.smart.dustbin.utils.StaticData;


public class SmartBinApp extends MultiDexApplication {

    public static Context mContext;
    private static SmartBinApp mInstance;
    private String TAG = SmartBinApp.class.getName();
    private ApiService mApiService;


    public static Context getAppContext() {
        return mContext;
    }

    public static SmartBinApp getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        MultiDex.install(this);
        super.onCreate();
        mContext = this;
        mInstance = this;
        basicSetupForAPI();
    }

    public ApiService getApiService() {
        return mApiService;
    }


    private void basicSetupForAPI() {

        NetworkUtility.Builder networkBuilder = new NetworkUtility.Builder(StaticData.BASE_URL)
                .withAccesToken(PreferenceUtils.getToken(mContext));
        networkBuilder.withConnectionTimeout(60)
                .withReadTimeout(10)
                .withShouldRetryOnConnectionFailure(true)
                .build();


        mApiService = networkBuilder.getRetrofit().create(ApiService.class);

    }

}
