package com.smart.dustbin;

import android.databinding.DataBindingUtil;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.smart.dustbin.base.BaseActivity;
import com.smart.dustbin.databinding.ActivityMainBinding;
import com.smart.dustbin.ui.frag.BinListFragment;
import com.smart.dustbin.ui.frag.BinMapFragment;

public class MainActivity extends BaseActivity implements TabLayout.BaseOnTabSelectedListener {

    private ActivityMainBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        binding.tabLayout.addTab(binding.tabLayout.newTab().setText("List"));
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText("Map"));


        binding.tabLayout.addOnTabSelectedListener(this);
        replaceFragment(R.id.frame_container,new BinListFragment(), null);
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        if (tab.getText().equals("List")) {
            showToast("List click");
            replaceFragment(R.id.frame_container,new BinListFragment(), null);
        }else{
            showToast("map click");
            replaceFragment(R.id.frame_container,new BinMapFragment(), null);
        }
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }
}

