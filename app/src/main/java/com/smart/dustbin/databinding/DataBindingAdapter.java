package com.smart.dustbin.databinding;

import android.content.Context;
import android.content.Intent;
import android.databinding.BindingAdapter;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.media.MediaMetadataRetriever;
import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.webkit.WebView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.smart.dustbin.R;
import com.smart.dustbin.utils.Constants;


import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.TimeZone;

import javax.annotation.Nullable;

public class DataBindingAdapter {

    public static final String TAG = DataBindingAdapter.class.getSimpleName();

    @BindingAdapter({"bind:font"})
    public static void setFont(TextView textView, String fontName) {
        Log.e("#FONT#", "" + fontName);
        textView.setTypeface(Typeface.createFromAsset(textView.getContext().getAssets(), "fonts/" + fontName));
    }

    @BindingAdapter({"bind:titlecase"})
    public static void setTitleCase(TextView textView, String text) {
        String result = "";
        if (!TextUtils.isEmpty(text)) {
            text = Html.fromHtml(text).toString();
            char firstChar = text.charAt(0);
            char firstCharToUpperCase = Character.toUpperCase(firstChar);
            result = result + firstCharToUpperCase;
            for (int i = 1; i < text.length(); i++) {
                char currentChar = text.charAt(i);
                char previousChar = text.charAt(i - 1);
                if (previousChar == ' ') {
                    char currentCharToUpperCase = Character.toUpperCase(currentChar);
                    result = result + currentCharToUpperCase;
                } else {
                    char currentCharToLowerCase = Character.toLowerCase(currentChar);
                    result = result + currentCharToLowerCase;
                }
            }
            textView.setText(result);
        }
    }


    @BindingAdapter({"bind:setPrice", "bind:currency"})
    public static void setPrice(TextView textView, String setPrice, String currenty_type) {
        StringBuffer result = new StringBuffer();
        if (!TextUtils.isEmpty(setPrice)) {
            try {
                setPrice = new DecimalFormat("0.00").format(Double.parseDouble(setPrice));
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
        if (!TextUtils.isEmpty(setPrice)) {
            currenty_type = Html.fromHtml(currenty_type).toString();
            result.append(currenty_type).append(" ").append(setPrice);
            textView.setText(result.toString());
        }
    }

    @BindingAdapter({"bind:setStatusColor"})
    public static void bindStatusColor(ImageView view, int r) {
        switch (r) {
            case 1:
                view.setImageDrawable(ContextCompat.getDrawable(view.getContext(),R.drawable.ic_brightness_blue_24dp));
                break;
            case 2:
                view.setImageDrawable(ContextCompat.getDrawable(view.getContext(),R.drawable.ic_brightness_red));
                break;
            case 3:
                view.setImageDrawable(ContextCompat.getDrawable(view.getContext(),R.drawable.ic_brightness_yellow));
                break;
            case 4:
                view.setImageDrawable(ContextCompat.getDrawable(view.getContext(),R.drawable.ic_brightness_green));
                break;
        }
    }

    @BindingAdapter({"bind:loadImage"})
    public static void setImageUrl(ImageView imageView, String imageUrl) {
        if (!TextUtils.isEmpty(imageUrl)) {
            Glide.with(imageView.getContext())
                    .load(imageUrl)
                    .placeholder(R.drawable.ic_image_black_24dp)
                    .error(R.drawable.ic_image_black_24dp)
                    .into(imageView)
            ;
        }
    }

    @BindingAdapter({"bind:loadImage", "bind:resize_value"})
    public static void setImageUrl(ImageView imageView, String imageUrl, int resize) {
        if (!TextUtils.isEmpty(imageUrl)) {
            Glide.with(imageView.getContext())
                    .load(imageUrl).override(resize, resize)
                    .placeholder(R.drawable.ic_image_black_24dp)
                    .error(R.drawable.ic_image_black_24dp)
                    .into(imageView);
        }
    }

    @BindingAdapter({"bind:date", "bind:date_format"})
    public static void setDate(TextView view, long timeStamp, String dateFormat) {

        view.setText("");
        String finalDateTime = "";
        if (timeStamp > 0) {
            Date _date = new Date(timeStamp * 1000L);
            if (_date != null) {
                finalDateTime = new SimpleDateFormat(dateFormat, Locale.ENGLISH).format(_date);
                view.setText(finalDateTime);
            }
        }
    }

    @BindingAdapter({"bind:date", "bind:date_format"})
    public static void setDate(TextView view, String timeStamp, String dateFormat) {
        view.setText("");
        String finalDateTime = "";
        if (!TextUtils.isEmpty(timeStamp)) {
            Date _date = new Date(Long.parseLong(timeStamp) * 1000L);
            if (_date != null) {
                finalDateTime = new SimpleDateFormat(dateFormat, Locale.ENGLISH).format(_date);
                view.setText(finalDateTime);
            }
        }
    }


    @BindingAdapter({"bind:html_text"})
    public static void setHtmlText(TextView textView, String html_text) {
        if (!TextUtils.isEmpty(html_text)) {
            textView.setText(Html.fromHtml(html_text));
        }
    }

    @BindingAdapter({"bind:html_text"})
    public static void setHtmlText(WebView webView, String html_text) {
        if (!TextUtils.isEmpty(html_text)) {
            String mimeType = "text/html";
            final String encoding = "UTF-8";
            webView.loadDataWithBaseURL("", html_text, mimeType, encoding, "");
        }
    }


    @BindingAdapter({"bind:set_video_first_frame"})
    public static void setVideoFirstFrame(ImageView imageView, String imageurl) {
        if (!TextUtils.isEmpty(imageurl)) {
            /* RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.drawable.box_active);2
            requestOptions.error(R.drawable.box_active);
            try {

                Glide.with(imageView.getContext())
                        .load(imageurl).apply(requestOptions)
                        .thumbnail(Glide.with(imageView.getContext()).load(imageView))
                        .into(imageView);
            } catch (Exception e) {
                e.printStackTrace();
            }*/

            ImageLoadingClass imageLoadingClass = new ImageLoadingClass(imageView, imageurl);
            imageLoadingClass.execute();

          /*  BitmapPool bitmapPool = Glide.get(imageView.getContext()).getBitmapPool();
            int microSecond = 1000000;// 6th second as an example
            VideoBitmapDecoder videoBitmapDecoder = new VideoBitmapDecoder(microSecond);
            FileDescriptorBitmapDecoder fileDescriptorBitmapDecoder = new FileDescriptorBitmapDecoder(videoBitmapDecoder, bitmapPool, DecodeFormat.PREFER_ARGB_8888);
            Glide.with(imageView.getContext())
                    .load(imageurl)
                    .asBitmap()
                    .listener(new RequestListener<String, Bitmap>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                            imageView.setImageDrawable(ContextCompat.getDrawable(imageView.getContext(), R.drawable.ic_close_black_24dp));
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            imageView.setImageBitmap(resource);
                            return false;
                        }
                    })
                    //.override(50,50)// Example
                    .videoDecoder(fileDescriptorBitmapDecoder)
                    .into(imageView);*/
        }
    }

    @BindingAdapter({"bind:src_url", "bind:crop_circle", "bind:default_placeHolder"})
    public static void setSrcUrlWithDefault(final ImageView view, final String sourceUrl, final boolean cropCircle, final Drawable defaultPlaceHolder) {
        //if (StringUtility.validateString(sourceUrl)) {
        // Log.e(TAG, " is crop circle " + cropCircle);
        Log.e(TAG, "image set with default placeholder called for imagesoruce : " + sourceUrl);

        if (view.getVisibility() == View.GONE && !TextUtils.isEmpty(sourceUrl)) {
            view.setVisibility(View.VISIBLE);
        }

        if (!TextUtils.isEmpty(sourceUrl)) {

            if (cropCircle) {
                Glide.with(view
                        .getContext())
                        .load(sourceUrl)
                        .placeholder(defaultPlaceHolder)
                        .transform(new CircleTransform(view.getContext()))
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
//                    .crossFade()
//                    .animate(android.R.anim.fade_in)
                        .into(new SimpleTarget<GlideDrawable>() {
                            @Override
                            public void onLoadStarted(Drawable placeholder) {
                                super.onLoadStarted(placeholder);
                                view.setImageDrawable(defaultPlaceHolder);
                            }

                            @Override
                            public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                                view.setVisibility(View.VISIBLE);
                                view.setImageDrawable(resource);
                            }

                            @Override
                            public void onLoadFailed(Exception e, Drawable errorDrawable) {
                                super.onLoadFailed(e, errorDrawable);
                                view.setVisibility(View.VISIBLE);
                                view.setImageDrawable(defaultPlaceHolder);
                            }
                        });
            } else {
                Glide.with(view.getContext())
                        .load(sourceUrl)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .placeholder(defaultPlaceHolder)
                        //.error(R.drawable.no_preview)
//                    .crossFade()
//                    .animate(android.R.anim.fade_in)
                        .into(new SimpleTarget<GlideDrawable>() {
                            @Override
                            public void onLoadStarted(Drawable placeholder) {
                                super.onLoadStarted(placeholder);

                            }

                            @Override
                            public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {

                                view.setImageDrawable(resource);
                            }

                            @Override
                            public void onLoadFailed(Exception e, Drawable errorDrawable) {
                                super.onLoadFailed(e, errorDrawable);

                                view.setImageDrawable(defaultPlaceHolder);
                            }
                        });
            }
            //}
        } else {
            view.setImageDrawable(defaultPlaceHolder);
        }
    }

    @BindingAdapter({"bind:src_url"})
    public static void setSrcUrlWithDefault(final ImageView view, final String sourceUrl) {
        if (view.getVisibility() == View.GONE && !TextUtils.isEmpty(sourceUrl)) {
            view.setVisibility(View.VISIBLE);
        }
        if (!TextUtils.isEmpty(sourceUrl)) {
            Glide.with(view.getContext())
                    .load(sourceUrl)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.mipmap.ic_launcher)
                    .into(new SimpleTarget<GlideDrawable>() {
                        @Override
                        public void onLoadStarted(Drawable placeholder) {
                            super.onLoadStarted(placeholder);

                        }

                        @Override
                        public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                            view.setImageDrawable(resource);
                        }

                        @Override
                        public void onLoadFailed(Exception e, Drawable errorDrawable) {
                            super.onLoadFailed(e, errorDrawable);
                            view.setImageResource(R.mipmap.ic_launcher);
                        }
                    });
        } else {
            view.setImageResource(R.mipmap.ic_launcher);
        }
    }


   /* @BindingAdapter({"bind:url", "bind:title"})
    public static void openWebUrl(TextView textView, String url, String title) {

        if (TextUtils.isEmpty(url)) {
            textView.setVisibility(View.GONE);
        } else {
            textView.setVisibility(View.VISIBLE);
            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(textView.getContext(), WebViewActivity.class);
                    intent.putExtra(WebViewActivity.TitleExtra, title);
                    intent.putExtra(WebViewActivity.UrlExtra, url);

                    textView.getContext().startActivity(intent);
                }
            });

        }
    }*/


    public static class CircleTransform extends BitmapTransformation {
        public CircleTransform(Context context) {
            super(context);
        }

        private static Bitmap circleCrop(BitmapPool pool, Bitmap source) {
            if (source == null)
                return null;

            int size = Math.min(source.getWidth(), source.getHeight());
            int x = (source.getWidth() - size) / 2;
            int y = (source.getHeight() - size) / 2;

            // TODO this could be acquired from the pool too
            Bitmap squared = Bitmap.createBitmap(source, x, y, size, size);

            Bitmap result = pool.get(size, size, Bitmap.Config.ARGB_8888);
            if (result == null) {
                result = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888);
            }

            Canvas canvas = new Canvas(result);
            Paint paint = new Paint();
            paint.setShader(new BitmapShader(squared, BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP));
            paint.setAntiAlias(true);
            float r = size / 2f;
            canvas.drawCircle(r, r, r, paint);
            return result;
        }

        @Override
        protected Bitmap transform(BitmapPool pool, Bitmap toTransform, int outWidth, int outHeight) {
            return circleCrop(pool, toTransform);
        }

        @Override
        public String getId() {
            return getClass().getName();
        }
    }

    @BindingAdapter({"bind:setDate", "bind:outDateType"})
    public static void setDate(TextView view, String date, Integer dateType) {

        if (date == null || date.isEmpty()) {
            return;
        }
        if (date.charAt(date.length() - 1) == 'Z')
            date = date.substring(0, date.length() - 4);
        SimpleDateFormat formatter = new SimpleDateFormat(Constants.DateFormat_YYYY_MM_DDTHHMMSS_SSS);

        Date value = null;
        long millisecond = 0;
        try {
            //2019-09-11T08:33:44.251329Z
            //2018-10-02T09:20:09Z
            //2019-09-16T13:38:33.360798Z
            value = formatter.parse(date);
            millisecond = value.getTime() + Long.parseLong("0");

            SimpleDateFormat dateFormatter;
            if (dateType == 0)
                dateFormatter = new SimpleDateFormat(Constants.DateFormat_EEEDYY);
            else if (dateType == 1)
                dateFormatter = new SimpleDateFormat(Constants.DateFormat_3);
            else
                dateFormatter = new SimpleDateFormat(Constants.DateFormat_EEEDYY);
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(millisecond);
            view.setText(dateFormatter.format(calendar.getTime()));
        } catch (ParseException e) {
            e.printStackTrace();
            view.setText(date);
        }


    }

    @BindingAdapter({"bind:date", "bind:currentFormat", "bind:requiredFormat"})
    public static void formatDate(TextView view, String date, String currentFormat, String requiredFormat) {

        String output = "";
        SimpleDateFormat currentSimpleDateFormat = null, requiredSimpleDateFormat = null;
        try {
            currentSimpleDateFormat = new SimpleDateFormat(currentFormat);
            currentSimpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

            requiredSimpleDateFormat = new SimpleDateFormat(requiredFormat);
            requiredSimpleDateFormat.setTimeZone(TimeZone.getDefault());

            output = requiredSimpleDateFormat.format(currentSimpleDateFormat.parse(date));

        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
        if (TextUtils.isEmpty(output)) {
            output = "Pending";
        }
        view.setText(output);
    }

    @BindingAdapter({"bind:inpText"})
    public static void setInputText(TextView textView, String input_text) {
        if (!TextUtils.isEmpty(input_text)) {
            textView.setText(input_text);
        }
    }

    public static class ImageLoadingClass extends AsyncTask<Void, Bitmap, Bitmap> {

        public ImageView imageView;
        public String imageUrl;

        public ImageLoadingClass(ImageView imageView, String imageUrl) {
            this.imageView = imageView;
            this.imageUrl = imageUrl;
        }

        @Override
        protected Bitmap doInBackground(Void... voids) {

            try {
                MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();

                mediaMetadataRetriever.setDataSource(imageUrl, new HashMap<String, String>());
                Bitmap bmFrame = mediaMetadataRetriever.getFrameAtTime(1000); //unit in microsecond
                return bmFrame;
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
                return null;
            } catch (Exception e) {
                return null;
            }

        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            if (bitmap == null)
                return;

            super.onPostExecute(bitmap);
            imageView.setImageBitmap(bitmap);
        }
    }
}
