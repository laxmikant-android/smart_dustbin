package com.smart.dustbin.base;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class BaseViewPagerFragmentArrayAdapter extends FragmentStatePagerAdapter {

    private final FragmentManager fm;
    List<? extends BaseFragment> fragments;
    Context context;
    ArrayList<String> titleList;
    public BaseViewPagerFragmentArrayAdapter(Context context, FragmentManager fm, List<? extends BaseFragment> fragments) {
        super(fm);
        this.fm = fm;
        this.context = context;
        this.fragments = fragments;
    }
 public BaseViewPagerFragmentArrayAdapter(Context context, FragmentManager fm, List<? extends
         BaseFragment> fragments, ArrayList<String> titleList) {
        super(fm);
        this.fm = fm;
        this.context = context;
        this.fragments = fragments;
        this.titleList = titleList;
    }

    @Override
    public Fragment getItem(int position) {
        if (fragments != null) {
            return fragments.get(position);
        }
        return null;
    }

    @Override
    public int getCount() {
        return fragments != null ? fragments.size() : 0;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return titleList!= null ? titleList.get(position): "";
    }

    public FragmentManager getFm() {
        return fm;
    }
}
