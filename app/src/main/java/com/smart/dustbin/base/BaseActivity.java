package com.smart.dustbin.base;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;


import com.smart.dustbin.BuildConfig;
import com.smart.dustbin.R;
import com.smart.dustbin.ui.customviews.SimpleDialogFragment;
import com.smart.dustbin.utils.AppUtils;
import com.smart.dustbin.utils.IAlertDialogActionListener;
import com.smart.dustbin.utils.MySharedPreference;
import com.smart.dustbin.utils.StaticData;

import java.io.File;
import java.io.IOException;
import java.util.Objects;


/**
 * Created by kipl146 on 5/9/2017.
 */

public class BaseActivity extends AppCompatActivity implements View.OnClickListener {

    public static final int REQUEST_CAPTURE = 111;
    public static final int REQUEST_GALLERY = 112;
    public MySharedPreference mPref;
    ProgressDialog progressDialog;
    Uri captureMediaFile = null;

    public static void setTranslucentStatusBar(Window window) {
        if (window == null) return;
        int sdkInt = Build.VERSION.SDK_INT;
        if (sdkInt < Build.VERSION_CODES.O) {
            if (sdkInt >= Build.VERSION_CODES.LOLLIPOP) {
                setTranslucentStatusBarLollipop(window);
            } else if (sdkInt >= Build.VERSION_CODES.KITKAT) {
                setTranslucentStatusBarKiKat(window);
            }
        } else {
            setStatusBarColor(window, android.R.color.transparent);
        }

    }

    public static void setStatusBarColor(Window window, int color) {
        int sdkInt = Build.VERSION.SDK_INT;
        if (sdkInt >= Build.VERSION_CODES.O) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(color);
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private static void setTranslucentStatusBarLollipop(Window window) {
        window.setStatusBarColor(
                window.getContext()
                        .getResources()
                        .getColor(android.R.color.transparent));
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    private static void setTranslucentStatusBarKiKat(Window window) {
        window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPref = new MySharedPreference(this);
    }

    /*
     * Display a short Toast on the current page
     * @param message : which is to be displayed
     */
    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public void showToast(int message) {
        Toast.makeText(this, getString(message), Toast.LENGTH_SHORT).show();
    }

    /*
     * Print log in the Monitor window
     * @param tag
     * @param message : which is to be displayed
     */
    public void log(String tag, String message) {
        if (StaticData.DEBUG) {
            Log.d(tag, message);
        }
    }

    /*
     * Adds/replaces a fragment within an activity
     * @param container : resource id of the container view within which the fragment is required to be added/replaced
     * @param fragment : which is to be added/replaced
     * @param arguments : bundled arguments required to be provided during fragment setup
     * */
    public void replaceFragment(@IdRes int container, Fragment fragment, Bundle arguments) {
        if (arguments != null) {
            fragment.setArguments(arguments);
        }
        getSupportFragmentManager().beginTransaction().replace(container, fragment).commitAllowingStateLoss();
    }

    public void showAlert(String message, String action3) {
        showAlert(message, "", "", action3);
    }

    public void showAlert(String message, String action1, String action2) {
        showAlert(message, action1, action2, "");
    }

    public void showAlert(String message, String action1, String action2, String action3) {
        showAlert(message, action1, action2, action3, null);
    }

    public void showAlert(String message, String action1, String action2, String action3, IAlertDialogActionListener iAlertDialogActionListener) {

        // DialogFragment.show() will take care of adding the fragment
        // in a transaction.  We also want to remove any currently showing
        // dialog, so make our own transaction and take care of that here.
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        // Create and show the dialog.
        SimpleDialogFragment dialog_fragment = SimpleDialogFragment.newInstance(getString(R.string.app_name), message, action1, action2, action3);
        if (iAlertDialogActionListener != null) {
            dialog_fragment.setIActionSelectionListener(iAlertDialogActionListener);
        }
        dialog_fragment.show(ft, "dialog");

    }

    public void showNetworkFailure() {

        // DialogFragment.show() will take care of adding the fragment
        // in a transaction.  We also want to remove any currently showing
        // dialog, so make our own transaction and take care of that here.
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        // Create and show the dialog.
        SimpleDialogFragment dialog_fragment = SimpleDialogFragment.newInstance(getString(R.string.app_name),
                this.getResources().getString(R.string.no_internet),
                this.getResources().getString(R.string.retry),
                this.getResources().getString(R.string.ok), "");

        dialog_fragment.setIActionSelectionListener(new IAlertDialogActionListener() {
            @Override
            public void onActionSelected(int actionType) {
                switch (actionType) {
                    case 1:
                        actionRetry();
                        break;
                    case 2:
                        acceptedNetworkFailure();
                        break;
                }
            }
        });
        dialog_fragment.show(ft, "dialog");

    }

    @Override
    protected void onResume() {
        super.onResume();
       // setVersionOnScreen();
    }

    public void hideKeyBoard() {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(findViewById(android.R.id.content).getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void handleBackButtonEvent(@NonNull final Toolbar toolbar) {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    protected void actionRetry() {

    }

    protected void acceptedNetworkFailure() {

    }

    public void configureToolBar(@NonNull final Toolbar toolbar) {
        setSupportActionBar(toolbar);
        getSupportActionBar().show();
        for (int i = 0; i < toolbar.getChildCount(); i++) {
            View view = toolbar.getChildAt(i);
            if (view instanceof TextView) {
                TextView textView = (TextView) view;
                textView.setGravity(View.TEXT_ALIGNMENT_CENTER);
                textView.setTextColor(ContextCompat.getColor(this, R.color.colorAccent));
            }
        }
    }

    //Loading Bar
    public void enableLoadingBar(boolean enable) {
        if (enable) {
            loadProgressBar(null, getString(R.string.loading), false);
        } else {
            dismissProgressBar();
        }
    }

    public void loadProgressBar(String title, String message, boolean cancellable) {
        if (progressDialog == null && !this.isFinishing()) {
            progressDialog = ProgressDialog.show(this, title, message, false, cancellable);
            progressDialog.setContentView(R.layout.custom_progress);
            progressDialog.getWindow().setBackgroundDrawable(new
                    ColorDrawable(Color.TRANSPARENT));
        }
    }

    public void dismissProgressBar() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        progressDialog = null;
    }

    public void setStatusbarcolor(int color_value) {
        Window window = getWindow();

// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(color_value);
    }

    public void setStatusbarTransperant() {
        Window w = getWindow(); // in Activity's onCreate() for instance
        w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams
                .FLAG_TRANSLUCENT_STATUS);
    }

    public void selectImage() {

        android.app.AlertDialog.Builder myAlertDialog = new android.app.AlertDialog.Builder(this);
        myAlertDialog.setTitle(getResources().getString(R.string.app_name));
        myAlertDialog.setMessage(getResources().getString(R.string.select_picture_mode));

        myAlertDialog.setPositiveButton(getResources().getString(R.string.gallery), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                checkForStoragePermissions();

            }
        });

        myAlertDialog.setNegativeButton(getResources().getString(R.string.camera), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                checkForCameraPermissions();
            }
        });
        myAlertDialog.show();
    }

    public void checkForStoragePermissions() {
        if (Build.VERSION.SDK_INT >= 23) {
            if ((ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) ||
                    (ActivityCompat.checkSelfPermission(this, Manifest.permission
                            .WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.READ_EXTERNAL_STORAGE) || ActivityCompat
                        .shouldShowRequestPermissionRationale(this,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                    openSetting(1);

                } else
                    requestPermissions(AppUtils.PERMISSIONS_STORAGE,
                            AppUtils.PERMISSIONS_REQUEST_STORAGE);
            } else {
                actionGallery();
            }
        } else {
            actionGallery();
        }
    }

    private void checkForCameraPermissions() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) !=
                    PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.CAMERA)) {
                    openSetting(2);
                } else
                    requestPermissions(AppUtils.PERMISSIONS_CAMERA, AppUtils.PERMISSIONS_REQUEST_CAMERA);
            } else {
//                actionCamera();
                pickImageFromCamera();

            }
        } else {
//            actionCamera();
            pickImageFromCamera();
        }
    }

    /*To get image from gallery*/
    private void actionGallery() {

        //
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        //

       /* Intent intent = new Intent();
        intent.setType("image*//*");
        intent.setAction(Intent.ACTION_GET_CONTENT);*/
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(Intent.createChooser(intent, getString(R.string
                    .SELECT_PICTURE)), REQUEST_GALLERY);
        } else {
            //  onInfo(getString(R.string.GALLERY_UNAVAILABLE));
        }
    }

    public void openSetting(int type) {

        getAlertDialogBuilder(getString(R.string.app_name), getString(R.string.permisson_req_msg,
                type == 1 ? " storage" : "camera"), getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getPackageName(), null);
                intent.setData(uri);
                startActivity(intent);
            }
        }, getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }, true).show();


    }


    //Pick Image From Camera
    public void pickImageFromCamera() {
        if (!askListOfPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_CAPTURE))
            return;
        Intent intent = new Intent();
        intent.setAction(MediaStore.ACTION_IMAGE_CAPTURE);
        try {
            File tempFile = File.createTempFile("image", ".png", new File(Objects.requireNonNull(AppUtils
                    .getTempMediaDirectory(this))));
//            captureMediaFile = Uri.fromFile(tempFile);
            captureMediaFile = FileProvider.getUriForFile(this, this.getApplicationContext().getPackageName() + ".provider", tempFile);

            intent.putExtra(MediaStore.EXTRA_OUTPUT, captureMediaFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        startActivityForResult(intent, REQUEST_CAPTURE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case AppUtils.PERMISSIONS_REQUEST_CAMERA:
                if (grantResults.length > 0) {
                    pickImageFromCamera();
                }
                break;
            case AppUtils.PERMISSIONS_REQUEST_STORAGE:
                if (grantResults.length > 0) {
                    actionGallery();
                }
                break;

        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    public boolean askListOfPermissions(String[] permissions, int requestCode) {
        boolean isOk = true;
        StringBuilder perNeed = new StringBuilder();
        for (String per : permissions) {
            if (!(ActivityCompat.checkSelfPermission(this, per) == PackageManager.PERMISSION_GRANTED)) {
                if (isOk)
                    isOk = false;
                perNeed.append(per);
                perNeed.append(",");
            }
        }
        if (isOk) {
            return true;
        }

        String reqPermissions = (perNeed.length() > 1 ? perNeed.substring(0, perNeed.length() - 1).toString() : "");
        if (!reqPermissions.isEmpty()) {
            String arrPer[] = reqPermissions.split(",");
            ActivityCompat.requestPermissions(this, permissions, requestCode);
        }
        return false;
    }

    public AlertDialog.Builder getAlertDialogBuilder(String title, String message, boolean cancellable) {
        return new AlertDialog.Builder(this, R.style.AlertDialogTheme)
                .setTitle(title)
                .setMessage(message)
                .setCancelable(cancellable);
    }

    public AlertDialog.Builder getAlertDialogBuilder(String title, String message, String
            positive_button, DialogInterface.OnClickListener positiveClick, String
                                                             negative_button, DialogInterface.OnClickListener negativeClick, boolean cancellable) {
        return new AlertDialog.Builder(this, R.style.AlertDialogTheme)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(positive_button, positiveClick)
                .setNegativeButton(negative_button, negativeClick)
                .setCancelable(cancellable);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CAPTURE) {
                getCameraImage(captureMediaFile);
            }

        }

    }

    public void getCameraImage(Uri uri) {

    }

    public void openUrl(String url) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

   /* public void openWeburl(String url, String title) {
        Intent intent = new Intent(this, WebViewActivity.class);
        intent.putExtra(WebViewActivity.TitleExtra, title);
        intent.putExtra(WebViewActivity.UrlExtra, url);

        startActivity(intent);
    }*/

    //Set Version on Screen
    public void setVersionOnScreen() {
        if (BuildConfig.DEBUG){
        TextView tvSample = new TextView(this);
        tvSample.setTextColor(Color.RED);
        tvSample.setPadding(4, 4, 4, 4);
        tvSample.setGravity(Gravity.BOTTOM);
        tvSample.setTextSize(8);
        tvSample.setText(BuildConfig.VERSION_NAME + "(" + 3 + ")");
        ((ViewGroup) findViewById(android.R.id.content)).addView(tvSample);
        }
    }

    @Override
    public void onClick(View v) {

    }






    public static int getNavBarHeight(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        DisplayMetrics realMetrics = new DisplayMetrics();
        display.getMetrics(metrics);
        display.getRealMetrics(realMetrics);
//      int physicalHeight = metrics.heightPixels;
//        Display.Mode mode =  display.getMode();
//        int physicalHeight = mode.getPhysicalHeight();
//        DisplayMetrics metrics = new DisplayMetrics();
//        display.getMetrics(metrics);
//        int height = metrics.heightPixels;
//        int diff= physicalHeight-height;
        return realMetrics.heightPixels - metrics.heightPixels;
    }

    public void handleVersionUpdateCase() {

        enableLoadingBar(false);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        SimpleDialogFragment dialog_fragment = SimpleDialogFragment.newInstance(getString(R.string.app_name),
                this.getResources().getString(R.string.force_update),
                "", "",
                this.getResources().getString(R.string.update));

        dialog_fragment.setIActionSelectionListener(new IAlertDialogActionListener() {
            @Override
            public void onActionSelected(int actionType) {
              //  startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(StaticData.APP_LINK)));
            }
        });
        dialog_fragment.show(ft, "dialog");

    }

}
