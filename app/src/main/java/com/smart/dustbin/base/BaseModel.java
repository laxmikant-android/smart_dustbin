package com.smart.dustbin.base;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.content.ContextCompat;

import com.smart.dustbin.BR;

public class BaseModel extends BaseObservable implements Parcelable {

    public BaseModel() {
    }

    protected BaseModel(Parcel in) {
        selected = in.readByte() != 0;
        txtHighLightColor = in.readInt();
    }


    public boolean selected = false;


    public static final Creator<BaseModel> CREATOR = new Creator<BaseModel>() {
        @Override
        public BaseModel createFromParcel(Parcel in) {
            return new BaseModel(in);
        }

        @Override
        public BaseModel[] newArray(int size) {
            return new BaseModel[size];
        }
    };

    public int getTxtHighLightColor(Context context) {
        return ContextCompat.getColor(context, txtHighLightColor);
    }

    public void setTxtHighLightColor(int txtHighLightColor) {
        this.txtHighLightColor = txtHighLightColor;
        //notifyPropertyChanged(com.reveel.BR.txtHighLightColor);
    }

    public int txtHighLightColor = android.R.color.white;

    @Bindable
    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
        notifyPropertyChanged(com.smart.dustbin.BR.selected);
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeByte((byte) (selected ? 1 : 0));
        parcel.writeInt(txtHighLightColor);
    }

}
