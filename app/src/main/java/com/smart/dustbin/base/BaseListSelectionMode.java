package com.smart.dustbin.base;

/**
 * Created by Admin on 27-12-2016.
 */

public enum BaseListSelectionMode {
    NONE, SINGLE, MULTIPLE
}
