package com.smart.dustbin.base;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.os.Parcel;
import android.os.Parcelable;

import com.smart.dustbin.BR;


/**
 * Created by Admin on 01-10-2016.
 */

public class BaseDataEntity extends BaseObservable implements Parcelable {

    public static final Creator<BaseDataEntity> CREATOR = new Creator<BaseDataEntity>() {
        @Override
        public BaseDataEntity createFromParcel(Parcel in) {
            return new BaseDataEntity(in);
        }

        @Override
        public BaseDataEntity[] newArray(int size) {
            return new BaseDataEntity[size];
        }
    };
    public boolean selected = false;
    public int position = 0;

    public BaseDataEntity() {
    }

    protected BaseDataEntity(Parcel in) {
        this.selected = in.readByte() != 0;
        this.position = in.readInt();
    }

    @Bindable
    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
        notifyPropertyChanged(com.smart.dustbin.BR.selected);
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(this.selected ? (byte) 1 : (byte) 0);
        dest.writeInt(position);
    }

}
