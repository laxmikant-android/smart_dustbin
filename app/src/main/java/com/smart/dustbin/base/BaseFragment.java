package com.smart.dustbin.base;

import android.app.ProgressDialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.smart.dustbin.R;
import com.smart.dustbin.ui.customviews.SimpleDialogFragment;
import com.smart.dustbin.utils.IAlertDialogActionListener;
import com.smart.dustbin.utils.MySharedPreference;
import com.smart.dustbin.utils.StaticData;

/**
 * Created by kipl146 on 5/9/2017.
 */

public class BaseFragment extends Fragment {

    public MySharedPreference mPref;
    ProgressDialog progressDialog;
    /*
     * Display a short Toast on the current page
     * @param message : which is to be displayed
     */

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mPref = new MySharedPreference(getActivity());
    }

    public void showToast(String message) {
        if (getActivity() != null) {
            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
        }
    }

    public void showToast(int message) {
        if (getActivity() != null) {
            Toast.makeText(getActivity(), getString(message), Toast.LENGTH_SHORT).show();
        }
    }

    /*
     * Print log in the Monitor window
     * @param tag
     * @param message : which is to be displayed
     */
    public void log(String tag, String message) {
        if (StaticData.DEBUG) {
            Log.d(tag, message);
        }
    }

    public void showAlert(String message, String action1) {
        showAlert(message, "", "", action1);
    }

    public void showAlert(String message, String action1, String action2) {
        showAlert(message, action1, action2, "");
    }

    public void showAlert(String message, String action1, String action2, String action3) {
        showAlert(message, action1, action2, action3, null);
    }

    public void showAlert(String message, String action1, String action2, String action3, IAlertDialogActionListener iAlertDialogActionListener) {

        if (getActivity() != null) {
            if (getActivity() instanceof BaseActivity) {
                ((BaseActivity) getActivity()).showAlert( message, action1, action2, action3, iAlertDialogActionListener);
            }
        }
    }

    public void showNetworkFailure() {

        if (getActivity() != null) {
            // DialogFragment.show() will take care of adding the fragment
            // in a transaction.  We also want to remove any currently showing
            // dialog, so make our own transaction and take care of that here.
            FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
            Fragment prev = getActivity().getSupportFragmentManager().findFragmentByTag("dialog");
            if (prev != null) {
                ft.remove(prev);
            }
            ft.addToBackStack(null);

            // Create and show the dialog.
            SimpleDialogFragment dialog_fragment = SimpleDialogFragment.newInstance(getString(R
                            .string.app_name),
                    this.getResources().getString(R.string.no_internet),
                    this.getResources().getString(R.string.retry),
                    this.getResources().getString(R.string.ok), "");

            dialog_fragment.setIActionSelectionListener(new IAlertDialogActionListener() {
                @Override
                public void onActionSelected(int actionType) {
                    switch (actionType) {
                        case 1:
                            actionRetry();
                            break;
                        case 2:
                            acceptedNetworkFailure();
                            break;
                    }
                }
            });
            dialog_fragment.show(ft, "dialog");
        }

    }

    public void replaceFragment(@IdRes final int container, final Fragment fragment) {
        final FrameLayout frameLayout = (FrameLayout) getActivity().findViewById(container);
        frameLayout.removeAllViewsInLayout();
        frameLayout.postDelayed(new Runnable() {
            @Override
            public void run() {
                getActivity().getSupportFragmentManager().beginTransaction().replace(frameLayout.getId(), fragment).addToBackStack(null).commitAllowingStateLoss();
                frameLayout.forceLayout();
            }
        }, 100);
    }

    protected void actionRetry() {

    }

    protected void acceptedNetworkFailure() {

    }

    public void enableLoadingBar(boolean enable) {
        if (enable) {
            loadProgressBar(null, getString(R.string.loading), false);
        } else {
            dismissProgressBar();
        }
    }

    public void loadProgressBar(String title, String message, boolean cancellable) {
        if (progressDialog == null && getActivity() != null){
            progressDialog = ProgressDialog.show(getActivity(), title, message, false, cancellable);
            progressDialog.setContentView(R.layout.custom_progress);
            progressDialog.getWindow().setBackgroundDrawable(new
                    ColorDrawable(android.graphics.Color.TRANSPARENT));
        }
    }

    public void dismissProgressBar() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        progressDialog = null;
    }



    public void openWeburl(String url, String title) {
       // ((BaseActivity) getActivity()).openWeburl(url, title);
    }

    public void handleVersionUpdateCase() {
        enableLoadingBar(false);
        ((BaseActivity) getActivity()).handleVersionUpdateCase();
    }

}