package com.smart.dustbin.models.data;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class BinListDataModel{

	@SerializedName("images")
	private List<String> images;

	@SerializedName("booking_start_date")
	private String bookingStartDate;

	@SerializedName("name")
	private String name;

	@SerializedName("lati")
	private double lati;

	@SerializedName("booking_end_data")
	private String bookingEndData;

	@SerializedName("remark")
	private String remark;

	@SerializedName("id")
	private int id;

	@SerializedName("longit")
	private double longit;

	@SerializedName("client_name")
	private String clientName;

	@SerializedName("status")
	private int status;

	public void setImages(List<String> images){
		this.images = images;
	}

	public List<String> getImages(){
		return images;
	}

	public void setBookingStartDate(String bookingStartDate){
		this.bookingStartDate = bookingStartDate;
	}

	public String getBookingStartDate(){
		return bookingStartDate;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setLati(double lati){
		this.lati = lati;
	}

	public double getLati(){
		return lati;
	}

	public void setBookingEndData(String bookingEndData){
		this.bookingEndData = bookingEndData;
	}

	public String getBookingEndData(){
		return bookingEndData;
	}

	public void setRemark(String remark){
		this.remark = remark;
	}

	public String getRemark(){
		return remark;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setLongit(double longit){
		this.longit = longit;
	}

	public double getLongit(){
		return longit;
	}

	public void setClientName(String clientName){
		this.clientName = clientName;
	}

	public String getClientName(){
		return clientName;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"BinListDataModel{" + 
			"images = '" + images + '\'' + 
			",booking_start_date = '" + bookingStartDate + '\'' + 
			",name = '" + name + '\'' + 
			",lati = '" + lati + '\'' + 
			",booking_end_data = '" + bookingEndData + '\'' + 
			",remark = '" + remark + '\'' + 
			",id = '" + id + '\'' + 
			",longit = '" + longit + '\'' + 
			",client_name = '" + clientName + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}