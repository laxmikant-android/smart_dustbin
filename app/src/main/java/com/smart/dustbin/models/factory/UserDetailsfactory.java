package com.smart.dustbin.models.factory;

import android.app.Application;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.smart.dustbin.models.repositories.UserDetailsRepository;
import com.smart.dustbin.viewmodel.UserDetailViewModel;

import javax.inject.Inject;

public class UserDetailsfactory  implements ViewModelProvider.Factory {

    @Inject
    UserDetailsRepository userRepository;
    Application mApplication;

    @Inject
    public UserDetailsfactory(Application application, UserDetailsRepository userRepository){
        mApplication = application;
        this.userRepository = userRepository;
    }


    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        if (modelClass.isAssignableFrom(UserDetailViewModel.class)) {
            return (T) new UserDetailViewModel(mApplication, userRepository);
        }
        throw new IllegalArgumentException("Wrong ViewModel class");
    }


}