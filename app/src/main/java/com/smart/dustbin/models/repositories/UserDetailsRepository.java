package com.smart.dustbin.models.repositories;

import android.content.Context;

import com.smart.dustbin.SmartBinApp;
import com.smart.dustbin.apis.ApiService;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;


public class UserDetailsRepository {

    private static UserDetailsRepository mUserDetailRepo;
    private static ApiService mApiService;
    private static Executor mExecutor;
    private static Context mAppContext;

    public synchronized static UserDetailsRepository getInstance(SmartBinApp appClass) {
        mAppContext = appClass.getApplicationContext();
        mUserDetailRepo = new UserDetailsRepository();
        mApiService = appClass.getApiService();
        mExecutor = Executors.newSingleThreadExecutor();
        return mUserDetailRepo;
    }

/*
    public LiveData<Resource<AuthTokenResponse>> loginUser(LoginRequest loginReq) {
        final MutableLiveData<Resource<AuthTokenResponse>> userDetails = new MutableLiveData<>();
        mExecutor.execute(() -> {
            mApiService.loginUser(loginReq).enqueue(new Callback<AuthTokenResponse>() {
                @Override
                public void onResponse(Call<AuthTokenResponse> call, Response<AuthTokenResponse> response) {
                    if (response.code() == 200) {
                        AuthTokenResponse user_details = response.body();
                        userDetails.setValue(new Resource<AuthTokenResponse>(SUCCESS, user_details, user_details.getMsg()));
                    } else if (response.code() == 400) {
                        userDetails.setValue(new Resource<>(ERROR, null, "Unable to log in with provided credentials."));
                    } else {


                        userDetails.setValue(new Resource<>(ERROR, null, mAppContext.getString(R.string.server_error)));
                    }
                }

                @Override
                public void onFailure(Call<AuthTokenResponse> call, Throwable t) {
                    userDetails.setValue(new Resource<>(ERROR, null, mAppContext.getString(R.string.server_error)));
                }
            });
        });
        return userDetails;
    }
*/

}