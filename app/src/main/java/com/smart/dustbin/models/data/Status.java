package com.smart.dustbin.models.data;

public enum Status {
    SUCCESS,
    ERROR,
    LOADING,
    FORCE_UPDATE
}
