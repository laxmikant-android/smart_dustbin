package com.smart.dustbin.apis.requests;


import com.google.gson.annotations.SerializedName;


public class ActualAssesmentRequest{

	@SerializedName("assessment")
	private int assessment;

	public void setAssessment(int assessment){
		this.assessment = assessment;
	}

	public int getAssessment(){
		return assessment;
	}
}