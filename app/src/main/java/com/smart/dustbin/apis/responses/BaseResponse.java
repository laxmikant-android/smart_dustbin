package com.smart.dustbin.apis.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.smart.dustbin.models.data.Status;
import com.smart.dustbin.utils.Constants;

/**
 * Created by kipl104 on 04-Apr-17.
 */

public class BaseResponse {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("product_count")
    @Expose
    private Integer product_count;
    @SerializedName("message")
    @Expose
    private String msg;
    @SerializedName("meta")
    @Expose
    private Meta meta;

    public Status getStatus() {

        if (status.equalsIgnoreCase(Constants.success)) {
            return Status.SUCCESS;
        } else if (status.equalsIgnoreCase(Constants.loading)) {
            return Status.LOADING;
        }
        return Status.ERROR;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public Integer getProduct_count() {
        return product_count;
    }

    public class Meta {
        @SerializedName("has_update")
        @Expose
        private String has_update;
        @SerializedName("force_update")
        @Expose
        private String force_update;
        @SerializedName("Version:")
        @Expose
        private String version;

        public String getHas_update() {
            return has_update;
        }

        public void setHas_update(String has_update) {
            this.has_update = has_update;
        }

        public String getForce_update() {
            return force_update;
        }

        public void setForce_update(String force_update) {
            this.force_update = force_update;
        }

        public String getVersion() {
            return version;
        }

        public void setVersion(String version) {
            this.version = version;
        }
    }
}
