package com.smart.dustbin.apis;

import java.util.List;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface ApiService {

  /*  @POST("/api/api-token-auth/")
    @Headers({"content-type: application/json", "Version:" + StaticData.HEADER_APP_VERSION, "language:" + StaticData.HEADER_LANGUAGE, "device_type:" + StaticData.HEADER_DEVICE_OS})
    Call<AuthTokenResponse> loginUser(@Body LoginRequest reqLogin);
*/
 /*   @POST("/api/users/userCreate/")
    @Headers({"content-type: application/json", "Version:" + StaticData.HEADER_APP_VERSION, "language:" + StaticData.HEADER_LANGUAGE, "device_type:" + StaticData.HEADER_DEVICE_OS})
    Call<SignUpRequest> signUpUser(@Body SignUpRequest reqLogin);

    @GET("/api/users/profile/")
    @Headers({"content-type: application/json", "Version:" + StaticData.HEADER_APP_VERSION, "language:" + StaticData.HEADER_LANGUAGE, "device_type:" + StaticData.HEADER_DEVICE_OS})
    Call<UserDetails> getUserDetails();

    @GET("/api/users/college/")
    @Headers({"content-type: application/json", "Version:" + StaticData.HEADER_APP_VERSION, "language:" + StaticData.HEADER_LANGUAGE, "device_type:" + StaticData.HEADER_DEVICE_OS})
    Call<List<ProfileSubCompoData>> getCollageList();

    @GET("/api/users/degree/")
    @Headers({"content-type: application/json", "Version:" + StaticData.HEADER_APP_VERSION, "language:" + StaticData.HEADER_LANGUAGE, "device_type:" + StaticData.HEADER_DEVICE_OS})
    Call<List<ProfileSubCompoData>> getDegreeList();

    @GET("/api/users/branch/")
    @Headers({"content-type: application/json", "Version:" + StaticData.HEADER_APP_VERSION, "language:" + StaticData.HEADER_LANGUAGE, "device_type:" + StaticData.HEADER_DEVICE_OS})
    Call<List<ProfileSubCompoData>> getBranchList();

    @GET("/api/course/activeCourseList/")
    @Headers({"content-type: application/json", "Version:" + StaticData.HEADER_APP_VERSION, "language:" + StaticData.HEADER_LANGUAGE, "device_type:" + StaticData.HEADER_DEVICE_OS})
    Call<List<CourceListDataModel>> getCourcesDetail();

    @GET("/api/course/lecture/{lecture_id}/")
    @Headers({"content-type: application/json", "Version:" + StaticData.HEADER_APP_VERSION, "language:" + StaticData.HEADER_LANGUAGE, "device_type:" + StaticData.HEADER_DEVICE_OS})
    Call<List<LectureListDataModel>> getLectureList(@Path("lecture_id") String lecture_id);

    @GET("/api/assessment/assessmentcreator/{id}/")
    @Headers({"content-type: application/json", "Version:" + StaticData.HEADER_APP_VERSION, "language:" + StaticData.HEADER_LANGUAGE, "device_type:" + StaticData.HEADER_DEVICE_OS})
    Call<AssesTestDetailDataModel> getAssesmentList(@Path("id") String id);

  *//* @GET("/api/results/getMyRank/")
    @Headers({"content-type: application/json", "Version:" + StaticData.HEADER_APP_VERSION, "language:" + StaticData.HEADER_LANGUAGE, "device_type:" + StaticData.HEADER_DEVICE_OS})
    Call<GetMyRankDataModel> getMyRank();*//*
*//*
    @GET("/aliafriq/wp-json/DEC/currency_list")
    @Headers({"Version:" + StaticData.HEADER_APP_VERSION,
            "language:" + StaticData.HEADER_LANGUAGE, "device_type:" + StaticData.HEADER_DEVICE_OS})
    Call<CurrencyResponse> getCurrencyList();*//*

    @POST("/api/coupons/applyCoupon")
    @Headers({"content-type: application/json", "Version:" + StaticData.HEADER_APP_VERSION, "language:" + StaticData.HEADER_LANGUAGE, "device_type:" + StaticData.HEADER_DEVICE_OS})
    Call<List<String>> applyCoupon(@Body ApplyCouponRequest request);

    @POST("/api/users/changePassword/")
    @Headers({"content-type: application/json", "Version:" + StaticData.HEADER_APP_VERSION, "language:" + StaticData.HEADER_LANGUAGE, "device_type:" + StaticData.HEADER_DEVICE_OS})
    Call<String> changePassword(@Body ChangePasswordRequest request);

    @GET("/api/assessment/results/")
    @Headers({"content-type: application/json", "Version:" + StaticData.HEADER_APP_VERSION, "language:" + StaticData.HEADER_LANGUAGE, "device_type:" + StaticData.HEADER_DEVICE_OS})
    Call<List<PerformanceDataModel>> getMyPerformanceResult();

   @GET("/api/assessment/result/{id}")
    @Headers({"content-type: application/json", "Version:" + StaticData.HEADER_APP_VERSION, "language:" + StaticData.HEADER_LANGUAGE, "device_type:" + StaticData.HEADER_DEVICE_OS})
    Call<AssesmentAnswerDataModel> getAssesmentIdResult(@Path("id") String id);

    @GET("/api/assessment/userAssessment/")
    @Headers({"content-type: application/json", "Version:" + StaticData.HEADER_APP_VERSION, "language:" + StaticData.HEADER_LANGUAGE, "device_type:" + StaticData.HEADER_DEVICE_OS})
    Call<List<AssesmentListDataModel>> getUserAssesment();

    @GET("/api/doubts/smeDoubts/")
    @Headers({"content-type: application/json", "Version:" + StaticData.HEADER_APP_VERSION, "language:" + StaticData.HEADER_LANGUAGE, "device_type:" + StaticData.HEADER_DEVICE_OS})
    Call<List<AskedDoughtDataModel>> getSMEDoubts();

    @GET("/api/course/category/")
    @Headers({"content-type: application/json", "Version:" + StaticData.HEADER_APP_VERSION, "language:" + StaticData.HEADER_LANGUAGE, "device_type:" + StaticData.HEADER_DEVICE_OS})
    Call<List<CourceCategoryDataModel>> getCourceCategory();

    @GET("/api/course/usercourses/")
    @Headers({"content-type: application/json", "Version:" + StaticData.HEADER_APP_VERSION, "language:" + StaticData.HEADER_LANGUAGE, "device_type:" + StaticData.HEADER_DEVICE_OS})
    Call<List<UserCourcesDatamodel>> getUserCources();

    @POST("/api/doubts/userDoubts/")
    @Headers({"content-type: application/json", "Version:" + StaticData.HEADER_APP_VERSION, "language:" + StaticData.HEADER_LANGUAGE, "device_type:" + StaticData.HEADER_DEVICE_OS})
    Call<AskDoubtResponse> getAskDoubt(@Body AskedDoubtRequest request);// response handle from 201

    @GET("/api/doubts/userDoubts/")
    @Headers({"content-type: application/json", "Version:" + StaticData.HEADER_APP_VERSION, "language:" + StaticData.HEADER_LANGUAGE, "device_type:" + StaticData.HEADER_DEVICE_OS})
    Call<List<AskDoubtResponse>> getAskDoubtList();// response handle from 201

    @POST("/api/course/purchaseCourse/")
    @Headers({"content-type: application/json", "Version:" + StaticData.HEADER_APP_VERSION, "language:" + StaticData.HEADER_LANGUAGE, "device_type:" + StaticData.HEADER_DEVICE_OS})
    Call<Void> getPurchaseCourse(@Body PurchaseCourceRequest request);// response handle from 201

    @POST("api/assessment/actualAssessment")
    @Headers({"content-type: application/json", "Version:" + StaticData.HEADER_APP_VERSION, "language:" + StaticData.HEADER_LANGUAGE, "device_type:" + StaticData.HEADER_DEVICE_OS})
    Call<AssesmentTestDataModel> getActualAssesment(@Body ActualAssesmentRequest request);// response handle from 201

    @POST("/api/assessment/submitAnswers/")
    @Headers({"content-type: application/json", "Version:" + StaticData.HEADER_APP_VERSION, "language:" + StaticData.HEADER_LANGUAGE, "device_type:" + StaticData.HEADER_DEVICE_OS})
    Call<Integer> getSubmitAnswers(@Body SubmitAnswareRequest request);// response handle from 201

    @PATCH("/api/course/updateProgress/")
    @Headers({"content-type: application/json", "Version:" + StaticData.HEADER_APP_VERSION, "language:" + StaticData.HEADER_LANGUAGE, "device_type:" + StaticData.HEADER_DEVICE_OS})
    Call<UserCourcesDatamodel> getUpdateProgress(@Body UpdateProgressRequest request);


    @Multipart
    @PUT("/api/users/profile/")
    @Headers({"Version:" + StaticData.HEADER_APP_VERSION, "language:" + StaticData.HEADER_LANGUAGE, "device_type:" + StaticData.HEADER_DEVICE_OS})
    Call<UserDetails> updateProfile(@Part("name") RequestBody name,
                                    @Part("usn") RequestBody usn,
                                    @Part("degree_start") RequestBody degree_start,
                                    @Part("mobile") RequestBody mobile,
                                    @Part("email") RequestBody email,
                                    @Part("college") RequestBody college,
                                    @Part("degree") RequestBody degree,
                                    @Part("branch") RequestBody branch,
                                    @Part("credits") RequestBody credits);
                                    *//*,@Part("photo") RequestBody image);*//*

            *//* @Body RequestBody requestBody*//*
                                    //@Part MultipartBody.Part photo);

*/
}
