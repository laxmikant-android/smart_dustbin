package com.smart.dustbin.adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.StringRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smart.dustbin.BR;
import com.smart.dustbin.base.BaseDataEntity;
import com.smart.dustbin.base.BaseListSelectionMode;
import com.smart.dustbin.base.BaseRecyclerViewArrayAdapter;

import java.util.List;

/**
 * Created by Admin on 01-09-2016.
 */
public class RecyclerViewArrayAdapter<T extends BaseDataEntity> extends BaseRecyclerViewArrayAdapter<T> {

    private int resLayout;
    private boolean isTitleVisible;

    public RecyclerViewArrayAdapter(Context context, final List<T> objects, int resLayout) {
        super(context, objects, null);
        this.resLayout = resLayout;
    }

    public RecyclerViewArrayAdapter(Context context, final List<T> objects, BaseRecyclerViewArrayAdapter.OnItemClickListener onItemClickListener, int resLayout) {
        super(context, objects, onItemClickListener, BaseListSelectionMode.NONE);
        this.resLayout = resLayout;
    }

/*
    public RecyclerViewArrayAdapter(Context context, final List<T> objects, OnItemClickListener onItemClickListener, OnWishListClickListener onWishListClickListener, int resLayout) {
        super(context, objects, onItemClickListener, onWishListClickListener,
                BaseListSelectionMode.NONE);
        this.resLayout = resLayout;
    }
*/

    public RecyclerViewArrayAdapter(Context context, final List<T> objects, BaseRecyclerViewArrayAdapter.OnItemOptionClickListener onItemOptionClickListener, int resLayout) {
        super(context, objects, onItemOptionClickListener, BaseListSelectionMode.NONE);
        this.resLayout = resLayout;
    }

    public RecyclerViewArrayAdapter(Context context, final List<T> objects, BaseRecyclerViewArrayAdapter.OnItemClickListener onItemClickListener,
                                    BaseRecyclerViewArrayAdapter.OnItemOptionClickListener onItemOptionClickListener, int resLayout) {
        super(context, objects, onItemClickListener, onItemOptionClickListener, BaseListSelectionMode.NONE);
        this.resLayout = resLayout;
    }

    public RecyclerViewArrayAdapter(Context context, final List<T> objects, BaseRecyclerViewArrayAdapter.OnItemClickListener onItemClickListener, BaseListSelectionMode selectionMode, int resLayout) {
        super(context, objects, onItemClickListener, selectionMode);
        this.resLayout = resLayout;
    }

    public void setIsTitleVisible(boolean isTitleVisible){
        this.isTitleVisible = isTitleVisible;
    }

    @Override
    public BaseRecyclerViewArrayAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(resLayout, parent, false);
        ViewDataBinding binding = DataBindingUtil.bind(v);
        return new BaseRecyclerViewArrayAdapter.MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(BaseRecyclerViewArrayAdapter.MyViewHolder holder, int position) {
        getItem(position).position = position;
       /* holder.getBinding().setVariable(com.smart.dustbin.BR.model, getItem(position));
        holder.getBinding().setVariable(BR.listItemClickListener, onItemClickListener);
        holder.getBinding().setVariable(BR.OnItemOptionClickListener, onItemOptionClickListener);
      */ /* holder.getBinding().setVariable(BR.listitem, getItem(position));
        holder.getBinding().setVariable(BR.listItemClickListener, onItemClickListener);
        holder.getBinding().setVariable(BR.listItemOptionClickListener, onItemOptionClickListener);
        holder.getBinding().setVariable(BR.wishListClickListener, onWishListClickListener);
        holder.getBinding().setVariable(BR.isTitleShow,isTitleVisible);
        holder.itemView.setTag(position);
        holder.getBinding().executePendingBindings();
        holder.getBinding().setVariable(BR.position, position);*/
    }

    @Override
    public int getItemViewType(int position) {
        return resLayout;
    }

    public T getItem(final int position) {
        return getmObjects().get(position);
    }

    public long getItemId(final int position) {
        return position;
    }

    public void setEmptyTextView(TextView emptyTextView, @StringRes int emptyViewText) {
        super.setEmptyTextView(emptyTextView, emptyViewText);
    }

}
