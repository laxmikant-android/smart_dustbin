package com.smart.dustbin.adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.smart.dustbin.base.BaseRecyclerViewArrayAdapter;

import java.util.List;

/**
 * Created by kipl146 on 6/23/2017.
 */

public class GeneralRecyclerViewAdapter<T extends Object> extends RecyclerView.Adapter<GeneralRecyclerViewAdapter.GeneralViewHolder> {

    private List<T> mObjects;
    private int resLayout;
    protected BaseRecyclerViewArrayAdapter.OnItemOptionClickListener<T> onListItemClickListener;
    private Context mContext;
    private T mObjectdata;

   /* public interface OnListItemClickListener<T> {
        void onListItemClicked(T object);
    }
*/
    public GeneralRecyclerViewAdapter(Context context, final List<T> objects, int resLayout,
                                      BaseRecyclerViewArrayAdapter.OnItemOptionClickListener onListItemClickListener) {
        mContext = context;
        mObjects = objects;
        this.resLayout = resLayout;
        this.onListItemClickListener = onListItemClickListener;
    }

    public void setObjects(T objectdata){
        this.mObjectdata= objectdata;
    }

    @Override
    public GeneralViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(resLayout, parent, false);
        ViewDataBinding binding = DataBindingUtil.bind(v);
        return new GeneralViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(GeneralViewHolder holder, int position) {

       /* holder.getBinding().setVariable(BR.model, mObjectdata);
        holder.getBinding().setVariable(BR.listitem, getItem(position));
        holder.getBinding().setVariable(BR.listItemOptionClickListener, onListItemClickListener);*/
        holder.itemView.setTag(position);
        holder.getBinding().executePendingBindings();

    }

    @Override
    public int getItemCount() {
        return mObjects.size();
    }

    public T getItem(final int position) {
        return getmObjects().get(position);
    }

    public List<T> getmObjects() {
        return mObjects;
    }

    public static class GeneralViewHolder extends RecyclerView.ViewHolder {

        ViewDataBinding binding;

        public GeneralViewHolder(final ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            this.binding.executePendingBindings();
        }

        public ViewDataBinding getBinding() {
            return binding;
        }

    }

}
